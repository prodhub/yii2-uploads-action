Yii2 Uploads Action
===================
For individual use.

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist platx/yii2-uploads-action "*"
```

or add

```
"platx/yii2-uploads-action": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, add this action to your controller  :

```php
public function actions()
{
    return [
        'uploads' => [
            'class' => 'platx\uploads\UploadsAction',
        ],
        ...
    ];
}
```

In your url rules add following rules:

```php
'uploads/<width:\d+>x<height:\d+>/<model:[\w_]+>/<field:[\w-]+>/<dp:[\d\/]+>/<file:[\w-]+>.<ext:\w+>' => '{your_controller}/uploads',
'uploads/original/<model:[\w_]+>/<field:[\w-]+>/<dp:[\d\/]+>/<file:[\w-]+>.<ext:\w+>' => '{your_controller}/uploads',
```

Nginx config:

```
location ~* /uploads/(\d+)/(\d+) {
    index index.php;
    try_files $uri $uri/ /index.php?$args;
}
```